# Trains

Zápočtový program pro předmět NPRG013 Programování v jazyce Java

## Jak spustit

```bash
# stažení
git clone git@gitlab.mff.cuni.cz:zakvojt/trains.git
cd trains

# spuštění
mvn compile exec:java

# spuštění z jaru
mvn clean compile assembly:single
java -jar target/trains-1.0-jar-with-dependencies.jar

# vygenerování dokumentace
mvn clean javadoc:javadoc
xdg-open target/site/apidocs/index.html
```

## Jak hrát

Po spuštění hry se otevře hlavní menu.
Tlačítko `NewGame` spustí novou hru.
Počáteční stav hry, města a tratě se dají nakonfigurovat v souboru `src/main/resources/world.yaml`.
`LoadGame` otevře menu, ve kterém se zobrazí uložené hry.
Hry se ukládají do `.yaml` souborů do složky `saves`, kde se také dajíí upravit.
Po kliknutí na uloženou hru se hra načte a zobrazí se mapa hry.
Tlačítko `Tutorial` zobrazí následující tutoriál.

Cílem hry je objevit města a železnice vedoucí mezi nimi.
Na mapě se zobrazují tratě a města s jejich názvy.
Neobjevená města a tratě jsou zobrazeny světle šedě, objevená černě.
V pravém horním rohu je zobrazen finanční stav.
Nová města a tratě se dají objevovat kupováním neobjevených tratí.
Železnice se dá koupit kliknutím na ní a potvrzením nákupu.

Vedle každého názvu města je zobrazen počet vlaků v onom městě.
Kliknutím na město se otevře menu města.
V něm se dá koupit nový vlak za 10000 korun.
Peníze se dají vydělat plněním zakázek, které se dají brát z menu města.
Na každou zakázku je potřeba 1 vlak.
Vlaky, které zrovna plní nějakou zakázku jsou na mapě zobrazeny modře.
