package cz.vojtechzak.trains.data;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.HashMap;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import cz.vojtechzak.trains.data.YamlSave.*;
import cz.vojtechzak.trains.entities.*;
import cz.vojtechzak.trains.jobs.Job;

/** Class loading Saves from yaml and storing Saves to yaml */
public class SaveHandler {
	
	/** YamlSave <-> yaml converter */
	private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

	/** initial game save url */
	private static URL defaultURL = SaveHandler.class.getResource("/world.yaml");

	private static Save save;
	private static HashMap<String, City> cityMap;

	/** Loads the Save from the default url */
	public static Save loadDefaultSave() {

		var saveName = LocalDateTime.now().toString() + ".yaml";
		var yamlSave = loadYaml(defaultURL);
		return yamlToSave(saveName, yamlSave);
	}

	/** Loads the Save with the given save name */
	public static Save loadSave(String saveName) {

		var url = getSaveURL(saveName);
		var yamlSave = loadYaml(url);
		return yamlToSave(saveName, yamlSave);
	}

	/**
	 * Stores the given save to the yaml file
	 * @param save the Save to store
	 */
	public static void storeSave(Save save) {

		save.played = LocalDateTime.now();
		var yamlSave = saveToYaml(save);
		try {
			mapper.writeValue(new File("saves/" + save.saveName), yamlSave);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loads yaml file into YamlSave
	 * @param url url of the yaml file
	 * @return loaded YamlSave
	 */
	private static YamlSave loadYaml(URL url) {
		var yamlSave = new YamlSave();
		try {
			yamlSave = mapper.readValue(url, YamlSave.class);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return yamlSave;
	}

	/**
	 * Converts the given YamlSave to Save
	 * @return the loaded Save
	 */
	private static Save yamlToSave(String saveName, YamlSave yamlSave) {

		save = new Save(saveName);
		cityMap = new HashMap<>();

		save.played = LocalDateTime.parse(yamlSave.played);
		save.balance = yamlSave.balance;

		for (var c : yamlSave.cities) {
			processYamlCity(c);
		}
		for (var t : yamlSave.tracks) {
			processYamlTrack(t);
		}
		for (var t : yamlSave.trains) {
			processYamlTrain(t);
		}

		return save;
	}

	/**
	 * Goes through the given YamlCity, checks its validity
	 * and creates a corresponding City 
	 */
	private static void processYamlCity(YamlCity c) {

		if (c.coordinates.length != 2) {
			System.err.println("City must have exactly 2 coordinates");
			return;
		}
		if (cityMap.containsKey(c.name)) {
			System.err.println("City " + c.name + " already exists");
			return;
		}

		City city = new City(c.name, c.coordinates[0], c.coordinates[1]);
		save.cities.add(city);
		cityMap.put(city.name, city);
	}

	/**
	 * Goes through the given YamlTrack, checks its validity
	 * and creates a corresponding Track 
	 */
	private static void processYamlTrack(YamlTrack t) {

		if (t.cities.length != 2) {
			System.err.println("Track must have exactly 2 endpoint cities");
			return;
		}

		City from = cityMap.get(t.cities[0]);
		City to = cityMap.get(t.cities[1]);

		if (from == null) {
			System.err.println("City " + t.cities[0] + " does not exist");
			return;
		}
		if (to == null) {
			System.err.println("City " + t.cities[1] + " does not exist");
			return;
		}

		Track track = new Track(from, to);
		if (t.explored) track.explore();

		save.tracks.add(track);
	}

	/**
	 * Goes through the given YamlTrain, checks its validity
	 * and creates a corresponding Train 
	 */
	private static void processYamlTrain(YamlTrain t) {

		var from = cityMap.get(t.from);
		if (from == null) {
			System.err.println("City " + t.from + "does not exist");
			return;
		}
		
		// train has no unfinished job
		if (t.to == null || t.departure == null) {
			var train = new Train(from);
			from.trains.add(train);
			save.trains.add(train);
			return;
		}

		var to = cityMap.get(t.to);
		if (to == null) {
			System.err.println("City " + t.to + "does not exist");
			return;
		}
		
		var departure = LocalDateTime.parse(t.departure);
		if (departure == null) {
			System.err.println("Invalid departure time " + t.departure);
			return;
		}

		var track = save.findTrack(from, to);
		if (track == null || !track.explored) {
			System.err.println("No discovered track is between " + t.from + " and " + t.to);
			return;
		}
		
		var job = new Job(track, t.priceCoef);
		save.trains.add(new Train(from, job, departure));
	}

	/** Converts the given Save to a YamlSave */
	public static YamlSave saveToYaml(Save save) {

		var yamlSave = new YamlSave();
		yamlSave.balance = save.balance;
		yamlSave.played = save.played.toString();
		yamlSave.cities = save.cities.stream()
			.map(c -> new YamlCity(c))
			.toArray(YamlCity[]::new);

		yamlSave.tracks = save.tracks.stream()
			.map(t -> new YamlTrack(t))
			.toArray(YamlTrack[]::new);
		
		yamlSave.trains = save.trains.stream()
			.map(t -> new YamlTrain(t))
			.toArray(YamlTrain[]::new);

		return yamlSave;
	}

	/** Returns the url of a save file with the given save name */
	private static URL getSaveURL(String saveName) {
		var url = defaultURL;
		try {
			url = new File("saves/",  saveName).toURI().toURL();
		}
		catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return url;
	}
}
