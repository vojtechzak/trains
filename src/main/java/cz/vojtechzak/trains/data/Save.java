package cz.vojtechzak.trains.data;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import cz.vojtechzak.trains.entities.*;

/** Class representing the current state of the game */
public class Save {

	/** file name of the save */
	public String saveName;

	public int balance = 20000;
	public LocalDateTime played;

	public List<City> cities = new ArrayList<>();
	public List<Track> tracks = new ArrayList<>();
	public List<Train> trains = new ArrayList<>();


	public Save(String saveName) {
		this.saveName = saveName;
	}

	public void save() {
		SaveHandler.storeSave(this);
	}

	/**
	 * Creates save description containing game progress 
	 * @return the created description
	 */
	public String getDescription() {
	
		long discovered = cities.stream().filter(c -> c.explored).count();
		String dateTime = played.format(DateTimeFormatter.ofPattern("dd. MM. yyyy HH:mm"));
		String template = "Last played %s, %d coins, %d trains, %d/%d cities discovered";
		return String.format(template, dateTime, balance, trains.size(), discovered, cities.size());
	}

	/**
	 * Looks for a track between the given cities, 
	 * the order does not matter
	 * @return track between the cities or null
	 */
	public Track findTrack(City c1, City c2) {

		return tracks.stream()
			.filter(t -> (t.from == c1 || t.to == c1))
			.filter(t -> (t.from == c2 || t.to == c2))
			.findFirst().orElse(null);
	}

	/**
	 * Looks at all trains that have ongoing jobs
	 * and completes them if they should be completed
	 */
	public void completeJobs() {

		trains.stream()
			.filter(t -> t.job != null)
			.forEach(t -> {
				if (t.isJobCompleted()) {
					balance += t.job.getPrice();
					t.jobFinished();
				}
			});
	}
}
