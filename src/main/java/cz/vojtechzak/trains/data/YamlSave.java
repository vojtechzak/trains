package cz.vojtechzak.trains.data;

import java.time.LocalDateTime;

import cz.vojtechzak.trains.entities.*;

/** Class representing a Save in the yaml form */
public class YamlSave {

	public int balance = 20000;
	public String played = LocalDateTime.now().toString();
	public YamlCity[] cities;
	public YamlTrack[] tracks;
	public YamlTrain[] trains = new YamlTrain[0];

	/** Class representing a city in the yaml form */
	static class YamlCity {

		public String name;
		public double[] coordinates;

		public YamlCity(){}

		/** Builds a YamlCity from the given City */
		public YamlCity(City city) {

			name = city.name;
			coordinates = new double[]{city.latitude, city.longitude};
		}
	}

	/** Class representing a track in the yaml form */
	static class YamlTrack {

		public String[] cities;
		public boolean explored = false;

		public YamlTrack(){}

		/** Builds a YamlTrack from the given Track */
		public YamlTrack(Track track) {

			cities = new String[]{track.from.name, track.to.name};
			explored = track.explored;
		}
	}

	/** Class representing a train in the yaml form */
	static class YamlTrain {

		public String from;
		public String to;
		public String departure;
		public int priceCoef;

		public YamlTrain(){}

		/** Builds a YamlTrain from the given Train */
		public YamlTrain(Train train) {

			from = train.lastCity.name;
			if (train.job != null) {
				to = train.otherCity.name;
				departure = train.departure.toString();
				priceCoef = train.job.priceCoef;
			}
		}
	}
}
