/**
 * Package containing classes working with the game saves stored in yaml files
 */
package cz.vojtechzak.trains.data;
