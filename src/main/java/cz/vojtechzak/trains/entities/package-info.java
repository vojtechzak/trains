/**
 * Package containing classes that represent entities displayed on the game map 
 */
package cz.vojtechzak.trains.entities;
