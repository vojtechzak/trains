package cz.vojtechzak.trains.entities;

import javax.swing.JButton;
import java.awt.geom.Line2D;
import java.awt.Graphics;
import java.awt.Color;

/** Class representing a track */
public class Track extends JButton {
	
	private static int THICKNESS = 6;

	public City from, to;
	public boolean explored = false;

	/** Creates a new Track between the given cities */
	public Track(City from, City to) {
		
		this.from = from;
		this.to = to;
	}

	/**
	 * Returns the second city on this track,
	 * that is not the same as the given City
	 */
	public City getOtherCity(City city) {
		if (from == city) return to;
		if (to == city) return from;
		return null;
	}

	/** Explores this track and both end cities */
	public void explore() {

		this.explored = true;
		from.explored = true;
		to.explored = true;
	}

	/**
	 * Calculates the length of this track
	 * @return the calculated length
	 */
	public int getLength() {

		int dx = from.x - to.x;
		int dy = from.y - to.y;
		return (int)Math.ceil(Math.sqrt(dx*dx + dy*dy));
	}

	/**
	 * Calculates the selling price of this track
	 * @return the calculated price
	 */
	public int getPrice() {
		return 20 * getLength();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(explored? Color.BLACK : Color.GRAY);
		g.drawLine(from.x, from.y, to.x, to.y);
	}

	@Override
	public boolean contains(int x, int y) {
		
		if (from.contains(x, y) || to.contains(x, y)) return false;
		var dist = Line2D.ptSegDist(from.x, from.y, to.x, to.y, x, y);
		return dist < THICKNESS;
	}
}
