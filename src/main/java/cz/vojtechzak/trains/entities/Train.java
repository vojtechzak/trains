package cz.vojtechzak.trains.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.time.Duration;
import java.time.LocalDateTime;
import javax.swing.JComponent;

import cz.vojtechzak.trains.jobs.Job;

/** Class representing a train */
public class Train extends JComponent {

	private static int RADIUS = 4;

	/** last city where this train was stationary */
	public City lastCity;

	/** current job, may be null */
	public Job job;

	/** departure time from the last city on the current job, may be null */
	public LocalDateTime departure;

	/** city where the train is headed on the current job, may be null */
	public City otherCity;

	/** Creates a train stationed in the given city */
	public Train(City lastCity) {
		this.lastCity = lastCity;
	}

	/** Creates a train on a job */
	public Train(City lastCity, Job job, LocalDateTime departure) {
		this.lastCity = lastCity;
		this.job = job;
		this.departure = departure;
		this.otherCity = job.track.getOtherCity(lastCity);
	}

	/** Sends this train on the given job */
	public void startJob(Job job) {

		lastCity.trains.remove(this);
		otherCity = job.track.getOtherCity(lastCity);
		this.job = job;
		this.departure = LocalDateTime.now();
	}
	
	/** Finds out, whether this train's job is completed */
	public boolean isJobCompleted() {

		if (job == null || departure == null) return false;
		return getTimeSinceDeparture() >= job.getDuration();
	}

	/** Finishes this train's job */
	public void jobFinished() {

		otherCity.trains.add(this);
		lastCity = otherCity;
		job = null;
		departure = null;
		otherCity = null;
	}

	/**
	 * Returns time elapsed since this trains departure
	 * @return time elapsed in miliseconds
	 */
	private long getTimeSinceDeparture() {
		return Duration.between(departure, LocalDateTime.now()).toMillis();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		if (job == null) return;
		
		float ratio = (float)getTimeSinceDeparture() / job.getDuration();
		float x = otherCity.x * ratio + lastCity.x * (1 - ratio);
		float y = otherCity.y * ratio + lastCity.y * (1 - ratio);
		
		g.setColor(Color.BLUE);
		g.fillOval((int)x - RADIUS, (int)y - RADIUS, 2*RADIUS, 2*RADIUS);
	}
}
