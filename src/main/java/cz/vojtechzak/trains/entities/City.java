package cz.vojtechzak.trains.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JButton;

/** Class representing a city */
public class City extends JButton {
	
	private static int RADIUS = 6;

	public String name;
	public int x, y;
	public double latitude, longitude;
	public boolean explored = false;
	public ArrayList<Train> trains = new ArrayList<>();

	/** Creates a new City with the given name and given coordinates */
	public City(String name, double latitude, double longitude) {

		this.name = name;
		this.latitude = latitude;
		this.longitude = longitude;
		loadPixelFromCoordinates();
	}

	/** Converts latitude and longitude into pixel coordinates */
	private void loadPixelFromCoordinates() {

		double SCALE_X = 189.1619304636152;
		double OFFSET_X = -2294.196312159608;
		double SCALE_Y = -270.07167657471024;
		double OFFSET_Y = 13812.056168857282;

		x = (int)(SCALE_X * longitude + OFFSET_X);
		y = (int)(SCALE_Y * latitude + OFFSET_Y);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(explored? Color.BLACK : Color.GRAY);
		g.fillOval(x - RADIUS, y - RADIUS, 2*RADIUS, 2*RADIUS);
		g.drawString(name + " (" + trains.size() + ")", x + RADIUS, y - RADIUS);
	}

	@Override
	public boolean contains(int x, int y) {

		int dx = this.x - x;
		int dy = this.y - y;
		return dx*dx + dy*dy <= RADIUS*RADIUS;
	}
}
