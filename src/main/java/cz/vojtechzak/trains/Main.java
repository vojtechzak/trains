package cz.vojtechzak.trains;

import java.io.File;

import cz.vojtechzak.trains.windows.Window;
import cz.vojtechzak.trains.windows.MainWindow;


/** Main class of the application */
public class Main {

	/** Creates the saves folder and displays a MainWindow */
	public static void main(String[] args) {

		var savesFile = new File("saves");
		if (!savesFile.exists()) {
			savesFile.mkdir();
		}

		Window.showWindow(new MainWindow());
	}
}
