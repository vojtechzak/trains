package cz.vojtechzak.trains.jobs;

import java.util.ArrayList;
import java.util.Random;

import cz.vojtechzak.trains.data.Save;
import cz.vojtechzak.trains.entities.City;


/** Class generating the jobs */
public class JobGenerator {
	
	private static Random random = new Random();

	/**
	 * Generates 1 to 3 jobs for each discovered track and each train
	 * @param save current game save
	 * @param city currently opened city
	 * @return arraylist of created jobs
	 */
	public static ArrayList<Job> generateJobs(Save save, City city) {

		var jobs = new ArrayList<Job>();
		
		save.tracks.stream()
			.filter(t -> t.from.explored && t.to.explored)
			.filter(t -> t.from == city || t.to == city)
			.forEach(t -> {
				for (int i = 0; i < city.trains.size(); i++) {
					// generate 1 to 3 jobs for each train
					int count = random.nextInt(1, 4);
					for (int j = 0; j < count; j++) {
						int priceCoef = random.nextInt(1, 5);
						jobs.add(new Job(t, priceCoef));
					}
				}
			});

		return jobs;
	}
}
