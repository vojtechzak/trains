package cz.vojtechzak.trains.jobs;

import cz.vojtechzak.trains.entities.*;

/** Class representing a job that the user can take to make coins */
public class Job {

	/** the track on which the job is to be completed */
	public Track track;

	/** coefficient multiplying the track length when calculating job price */
	public int priceCoef;

	public Job(Track track, int priceCoef) {
		
		this.track = track;
		this.priceCoef = priceCoef;
	}

	/**
	 * Creates a job description
	 * @param city the start city of the job
	 * @return description in form Praha -> Kladno, 65 coins
	 */
	public String getDescription(City city) {

		String otherCity = track.getOtherCity(city).name;
		return city.name + " -> " + otherCity + ", " + getPrice() + " coins";
	}

	/**
	 * Calculates the reward for completing the job
	 * @return price in coins
	 */
	public int getPrice() {
		return priceCoef * track.getLength();
	}

	/**
	 * Calculates how long the job takes to complete
	 * @return job duration in miliseconds
	 */
	public long getDuration() {
		return track.getLength() * 1000;
	}
}
