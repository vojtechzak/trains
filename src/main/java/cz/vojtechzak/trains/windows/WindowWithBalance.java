package cz.vojtechzak.trains.windows;

import javax.swing.JLabel;

import cz.vojtechzak.trains.data.Save;

/** Base class for all windows that have coin balance in top right corner */
public class WindowWithBalance extends Window {
	
	Save save;
	JLabel balanceLabel = new JLabel();

	/**
	 * Changes the coins balance, stores the save and reloads the window
	 * @param diff what amount should be added to balance
	 */
	public void changeBalanceAndSave(int diff) {
		save.balance += diff;
		save.save();
		reloadWindow();
	}

	@Override
	public void createComponent() {
		setLayout(null);
	
		balanceLabel.setBounds(1070, 10, 200, 25);
		add(balanceLabel);
	}

	@Override
	public void reloadComponent() {
		balanceLabel.setText("balance: " + save.balance + " coins");
	}
}
