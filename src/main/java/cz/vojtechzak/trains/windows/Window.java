package cz.vojtechzak.trains.windows;

import java.util.Stack;

import javax.swing.JFrame;
import javax.swing.JPanel;


/** Base class for all Windows */
public class Window extends JPanel {

	/** root frame of the application */
	private static JFrame frame = new RootFrame();

	/** root panel of the application */
	private static Window panel;

	/** panel stack */
	private static Stack<Window> windowStack = new Stack<>();

	/** Displays the given window, pushes the previous into the stack */
	public static void showWindow(Window window) {

		// add previous window to the stack
		if (panel != null) {
			panel.hideComponent();
			windowStack.push(panel);
			frame.remove(panel);
		}

		// display new window
		panel = window;
		panel.createComponent();
		frame.add(panel);

		reloadWindow();
	}

	/**
	 * Closes the window being currently shown, pops one from the stack. 
	 */
	public static void closeWindow() {

		// close the application if there is no window to display
		if (windowStack.empty()) {
			System.exit(0);
		}

		// switch to panel before
		panel.hideComponent();
		frame.remove(panel);
		panel = windowStack.pop();
		frame.add(panel);
		
		// reload the application frame
		reloadWindow();
	}

	/** Reloads the current window */
	public static void reloadWindow() {

		panel.reloadComponent();
		panel.revalidate();
		panel.repaint();

		frame.revalidate();
		frame.repaint();
	}

	/** This is called when the window is about to be displayed */
	public void createComponent(){};

	/**
	 * This is called when a different window is about to be displayed 
	 * and the current window is pushed to the stack
	 */
	public void hideComponent(){};

	/**
	 * This is called when a different window is closed
	 * and the current window popped from the stack
	 */	public void reloadComponent(){};
}

/** Root frame of the application */
class RootFrame extends JFrame {

	RootFrame() {

		setTitle("Trains");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setSize(1280, 760);
		setResizable(false);
		setLocationRelativeTo(null);

		setVisible(true);
	}
}
