package cz.vojtechzak.trains.windows;

import java.io.File;
import java.util.stream.Stream;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import cz.vojtechzak.trains.data.SaveHandler;

/** Load save menu of the games */
public class LoadGameWindow extends Window {

	JPanel savesPanel = new JPanel();

	@Override
	public void createComponent() {
		setLayout(null);

		var backButton = new JButton("Back to Menu");
		backButton.addActionListener(e -> {
			closeWindow();
		});
		backButton.setBounds(10, 10, 200, 25);
		add(backButton);

		var savesLabel = new JLabel("Saves:");
		savesLabel.setBounds(300, 10, 680, 25);
		add(savesLabel);

		var wrapperPanel = new JPanel();
		wrapperPanel.setLayout(new BoxLayout(wrapperPanel, BoxLayout.Y_AXIS));
		wrapperPanel.setBounds(300, 40, 680, 600);
		add(wrapperPanel);

		savesPanel.setLayout(new BoxLayout(savesPanel, BoxLayout.Y_AXIS));
		var scrollPanel = new JScrollPane(savesPanel);
		wrapperPanel.add(scrollPanel);
	}

	@Override
	public void reloadComponent() {

		savesPanel.removeAll();

		Stream.of(new File("saves").listFiles())
			.map(f -> f.getName())
			.map(n -> SaveHandler.loadSave(n))
			.sorted((s1, s2) -> s2.played.compareTo(s1.played))
			.forEach(s -> {

				var button = new JButton(s.getDescription());
				button.addActionListener(e -> {
					closeWindow();
					showWindow(new GameWindow(s));
				});
				savesPanel.add(button);
			});
	}
}
