package cz.vojtechzak.trains.windows;

import javax.swing.JButton;

/** Main menu of the game */
public class MainWindow extends Window {
	
	@Override
	public void createComponent() {

		var newGameButton = new JButton("New Game");
		newGameButton.addActionListener(e -> {
			showWindow(new GameWindow());
		});

		var loadGameButton = new JButton("Load Game");
		loadGameButton.addActionListener(e -> {
			showWindow(new LoadGameWindow());
		});
		
		var exitButton = new JButton("Exit");
		exitButton.addActionListener(e -> {
			closeWindow();
		});

		add(newGameButton);
		add(loadGameButton);
		add(exitButton);
	}
}
