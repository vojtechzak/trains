package cz.vojtechzak.trains.windows;

import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import cz.vojtechzak.trains.jobs.*;
import cz.vojtechzak.trains.data.Save;
import cz.vojtechzak.trains.entities.*;

/** Window showing the city menu */
public class CityWindow extends WindowWithBalance {
	
	City city;
	ArrayList<Job> jobs;

	JLabel cityLabel = new JLabel();
	JLabel trainsLabel = new JLabel();
	JLabel jobsLabel = new JLabel();

	JPanel jobsPanel = new JPanel();

	/** Creates a new CityWindow with the given city and save */
	CityWindow(City city, Save save) {

		this.city = city;
		this.save = save;
	}

	/** Called when the player clicks on a job */
	private void takeJob(Job job) {

		Train train = city.trains.remove(0);
		train.startJob(job);

		save.save();
		reloadWindow();
	}

	/** Called when the player tries to buy a new train */
	private void attemptTrainBuy() {

		int price = 10000;
		if (save.balance < price) {
			JOptionPane.showMessageDialog(this,
				"New train costs " + price + " coins, you only have " + save.balance + " coins.");
			return;
		}

		String template = "Do you want to buy a new train for %d coins?";
		var response = JOptionPane.showConfirmDialog(this, String.format(template, price));
		
		if (response == 0) {
			var train = new Train(city);
			save.trains.add(train);
			city.trains.add(train);
			changeBalanceAndSave(-price);
		}
	}

	@Override
	public void createComponent() {
		super.createComponent();

		createLabels();
		createButtons();

		var wrapperPanel = new JPanel();
		wrapperPanel.setLayout(new BoxLayout(wrapperPanel, BoxLayout.Y_AXIS));
		wrapperPanel.setBounds(300, 100, 680, 600);
		add(wrapperPanel);

		jobsPanel.setLayout(new BoxLayout(jobsPanel, BoxLayout.Y_AXIS));
		var scrollPanel = new JScrollPane(jobsPanel);
		wrapperPanel.add(scrollPanel);
	}

	/** Create and add labels to the window */
	private void createLabels() {

		cityLabel.setBounds(300, 10, 680, 25);
		trainsLabel.setBounds(300, 40, 680, 25);
		jobsLabel.setBounds(300, 70, 680, 25);

		add(cityLabel);
		add(trainsLabel);
		add(jobsLabel);
	}

	/** Create and add buttons to the window */
	private void createButtons() {

		var backButton = new JButton("Back to Map");
		backButton.addActionListener(e -> {
			closeWindow();
		});
		backButton.setBounds(10, 10, 200, 25);

		var buyTrainButton = new JButton("Buy new train");
		buyTrainButton.addActionListener(e -> {
			attemptTrainBuy();			
		});
		buyTrainButton.setBounds(1070, 40, 200, 25);

		add(backButton);
		add(buyTrainButton);
	}

	@Override
	public void reloadComponent() {
		super.reloadComponent();

		jobs = JobGenerator.generateJobs(save, city);

		cityLabel.setText(city.name);
		trainsLabel.setText("trains: " + city.trains.size());
		jobsLabel.setText("jobs available: " + jobs.size());

		jobsPanel.removeAll();

		jobs.stream()
			.sorted((j1, j2) -> j2.getPrice() - j1.getPrice())
			.forEach(j -> {
				var button = new JButton(j.getDescription(city));
				button.addActionListener(e -> {
					takeJob(j);
				});
				jobsPanel.add(button);
		});
	}
}
