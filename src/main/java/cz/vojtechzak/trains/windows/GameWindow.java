package cz.vojtechzak.trains.windows;

import java.awt.Image;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import cz.vojtechzak.trains.data.*;
import cz.vojtechzak.trains.entities.*;

/** Main game window showing the game map */
public class GameWindow extends WindowWithBalance {

	Image background = new ImageIcon(Window.class.getResource("/czechia.png"))
		.getImage().getScaledInstance(1280, 720, Image.SCALE_FAST);

	Timer trainTimer = new Timer(1000, e -> {
		save.completeJobs();
		reloadWindow();
	});

	/** Creates a new GameWindow with a default save */
	GameWindow() {
		this(SaveHandler.loadDefaultSave());	
	}
	
	/** Creates a new GameWindow with the given save */
	GameWindow(Save save) {
		this.save = save;
		this.save.completeJobs();
		this.save.save();
	}

	/** Called when the player clicks on the given track */
	private void attemptTrackBuy(Track track) {
		
		if (track.explored) return;

		int price = track.getPrice();

		var hasSomeTracks = save.tracks.stream().filter(t -> t.explored).count() > 0;
		var endCityExplored = track.from.explored || track.to.explored;

		if (hasSomeTracks && !endCityExplored) {
			JOptionPane.showMessageDialog(this, "You cannot buy a track between unexplored cities");
			return;
		}

		if (save.balance < price) {
			String message = "This track costs " + price + " coins, you only have " + save.balance;
			JOptionPane.showMessageDialog(this, message);
			return;
		}

		String template = "Do you want to buy track %s - %s for %d coins?";
		String message = String.format(template, track.from.name, track.to.name, price);
		var response = JOptionPane.showConfirmDialog(this, message);
		
		if (response == 0) {
			track.explore();
			changeBalanceAndSave(-price);
		}
	}

	@Override
	public void createComponent() {
		super.createComponent();

		createEntities();
		createButtons();
	}

	/** Creates and add cities, tracks and trains to the window */
	private void createEntities() {

		save.tracks.forEach(t -> {
			t.addActionListener(e -> attemptTrackBuy(t));
			add(t);
		});

		save.cities.forEach(c -> {
			c.addActionListener(e -> {
				if (c.explored) {
					showWindow(new CityWindow(c, save));
				}
			});
			add(c);
		});

		save.trains.forEach(t -> add(t));
	}

	/** Creates and adds buttons to the window */
	private void createButtons() {

		var exitButton = new JButton("Save & Exit");
		exitButton.addActionListener(e -> {
			save.save();
			System.exit(0);
		});
		exitButton.setBounds(10, 10, 200, 25);

		var backButton = new JButton("Save & Exit to Menu");
		backButton.addActionListener(e -> {
			save.save();
			closeWindow();
		});
		backButton.setBounds(10, 40, 200, 25);

		var tutorialButton = new JButton("Tutorial");
		tutorialButton.addActionListener(e -> {
			JOptionPane.showMessageDialog(this, TUTORIAL);
		});
		tutorialButton.setBounds(1070, 40, 200, 25);

		add(exitButton);
		add(backButton);
		add(tutorialButton);
	}

	@Override
	public void hideComponent() {
		super.hideComponent();
		trainTimer.stop();
	}

	@Override
	public void reloadComponent() {
		super.reloadComponent();
		trainTimer.start();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.drawImage(background, 0, 0, this);

		save.tracks.forEach(t -> t.paintComponent(g));
		save.trains.forEach(t -> t.paintComponent(g));
		save.cities.forEach(c -> c.paintComponent(g));
	}

	private static final String TUTORIAL = """

		Welcome to the Trains Game. This game is located in Czech Republic.
		Your task is to explore the railroads and cities of Czech Republic.

		On the map, you can see train tracks and cities with their names.
		Unexplored tracks and cities are displayed in light grey, while explored cities and tracks are black. 
		In the top right corner, you can see your coin balance.
		You can explore new tracks and cities by buying unexplored tracks.
		You can buy a new track by clicking on it and confirming the buy.

		Each city has a number next to it that displays the amount of trains currently stationed in the city.
		Clicking on an explored city opens the city window.
		In the city, new trains can be bought for 10k coins.
		You can make coins by taking jobs from the city window.
		Each job requires one train.
		Trains that are currently working on jobs are displayed on the map in blue.

		Enjoy!
	""";
}
